CREATE TABLE IF NOT EXISTS `<project_id>.Conhecimento_LOD.latam_tb_excep_ar_csr`
 (

code_sales_doc_exar  STRING  OPTIONS(DESCRIPTION="iden_document - Sales Document Number"),
nmbr_item_exar  INT64  OPTIONS(DESCRIPTION="iden_item - Item Number of Sales Document"),
desc_reason_exar  STRING  OPTIONS(DESCRIPTION="desc_reason - Description of the reason for exclusion"),
date_last_update_exar  TIMESTAMP  OPTIONS(DESCRIPTION="date_last_update - Date/Time BI Record Update"),

)
PARTITION BY DATE (date_last_update_exar)
CLUSTER   BY nmbr_item_exar
  OPTIONS (DESCRIPTION="[EXCEP_AR_CSR - MANUAL FILE] - This table contains the list of excluded sales orders from service rate KPI")
